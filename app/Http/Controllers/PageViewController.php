<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageViewController extends Controller
{
    public function showIndexPage()
    {
        return view('pages.index');
    }
}
