<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Meta Tags -->
    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <meta name="description" content="Edupoint - Nurturing minds, growing wealth. Your path as a school owner"/>
    <meta name="keywords" content="edupoint,manage,schools,free-of-use,nurturing,minds"/>
    <meta name="author" content="ThemeMascot"/>

    <title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
    <!-- Favicon and Touch Icons -->
    <link href="{{ asset('assets/images/favicon.png') }}" rel="shortcut icon" type="image/png">
    <link href="{{asset('assets/images/apple-touch-icon.png')}}" rel="apple-touch-icon">
    <link href="{{asset('assets/images/apple-touch-icon-72x72.png')}}" rel="apple-touch-icon" sizes="72x72">
    <link href="{{asset('assets/images/apple-touch-icon-114x114.png')}}" rel="apple-touch-icon" sizes="114x114">
    <link href="{{asset('assets/images/apple-touch-icon-144x144.png')}}" rel="apple-touch-icon" sizes="144x144">

    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/css-plugin-collections.css')}}" rel="stylesheet"/>

    <!-- CSS | menuzord megamenu skins -->
    <link href="{{asset('assets/css/menuzord-megamenu.css')}}" rel="stylesheet"/>
    <link id="menuzord-menu-skins" href="{{asset('assets/css/menuzord-skins/menuzord-boxed.css')}}" rel="stylesheet"/>
    <!-- CSS | Main style file -->
    <link href="{{asset('assets/css/style-main.css')}}" rel="stylesheet" type="text/css">
    <!-- CSS | Preloader Styles -->
    <link href="{{asset('assets/css/preloader.css')}}" rel="stylesheet" type="text/css">
    <!-- CSS | Custom Margin Padding Collection -->
    <link href="{{asset('assets/css/custom-bootstrap-margin-padding.css')}}" rel="stylesheet" type="text/css">
    <!-- CSS | Responsive media queries -->
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css">
    <!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
    <!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

    <!-- Revolution Slider 5.x CSS settings -->
    <link href="{{asset('assets/js/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/js/revolution-slider/css/layers.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/js/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css"/>

    <!-- CSS | Theme Color -->
    <link href="{{asset('assets/css/colors/theme-skin-color-set1.css')}}" rel="stylesheet" type="text/css">


    <script type="text/javascript"
            src="{{asset('assets/js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('assets/js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('assets/js/revolution-slider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('assets/js/revolution-slider/js/extensions/revolution.extension.parallax.min.js')}}"></script>

    <!-- Scripts -->

</head>

<body class="">
<div id="wrapper" class="clearfix">
    <!-- preloader -->
    <div id="preloader">
        <div id="spinner">
            <div class="preloader-dot-loading">
                <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
            </div>
        </div>
        <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
    </div>

@include('includes.header01')

<!-- Start main-content -->
    <div class="main-content">
        @yield('content')
    </div>

    @include('includes.footer01')
</div>

<script src="{{asset('assets/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="{{asset('assets/js/jquery-plugin-collection.js')}}"></script>
<!-- JS | Custom script for all pages -->
<script src="{{asset('assets/js/custom.js')}}"></script>
<script src="{{asset('assets/js/extra.js')}}"></script>

<!-- Revolution Slider 5.x SCRIPTS -->
<script src="{{asset('assets/js/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('assets/js/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('assets/js/extra-rev-slider-new.js')}}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
      (Load Extensions only on Local File Systems !
       The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript"
        src="{{asset('assets/js/revolution-slider/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('assets/js/revoluassets/tion-slider/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('assets/js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js')}}"></script>

<script type="text/javascript"
        src="{{asset('assets/js/revolution-slider/js/extensions/revolution.extension.migration.min.js')}}"></script>


<script type="text/javascript"
        src="{{asset('assets/js/revolution-slider/js/extensions/revolution.extension.video.min.js')}}"></script>
</body>

</html>
