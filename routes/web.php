<?php

use Illuminate\Support\Facades\Route;


Route::get('/', [\App\Http\Controllers\PageViewController::class, 'showIndexPage'])->name('home');

